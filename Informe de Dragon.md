# informe sobre DragonSpace

### Alumnos

- Juanmanuel Valenzuela
- Gastón villafañe

### Docentes

- René cejas bolecek
- Martín René


### Temas a desarrollar

#### Gastón Villafañe
- Descripción del módulo Dragon, versiones y propósitos.
- Aspectos del diseño: Escudo térmico y Sistema de control de reacción, potencia y recuperación.
- Misión de servicios de reabastecimiento comercial: CRS1 a 6
- Descripción del DragonLab

#### Juanmanuel Valenzuela
- Especificaciones del Dragon.
- Vinculación con la Estación Espacial Internacional: aspectos más importantes e hitos.
- Descripción de Programa de tripulación de la NASA.
- Evaluación de las potenciales demandas de ingeniería en el desarrollo de Dragon y qué materias/conocimientos asociados en su carrera podrían ajustarse más a las mismas.


# Descripción del módulo Dragon, versiones y propósitos
El SpaceX Dragon , también conocido como Dragon , es una nave espacial de carga reutilizable desarrollada por SpaceX , una compañía estadounidense de transporte espacial privado. Durante su primer vuelo en diciembre de 2010, Dragon se convirtió en la primera nave espacial construida y operada comercialmente que se recuperó con éxito de la órbita. Lo que a su vez se convirtio en la primera mision espacial realizda por una empresa privada.
El objetivo de SpaceX fue crear una nave a modo capsula capas de transportar personas, hacia y desde la orbita terrestre, que podia ser reutilizada una cantidad considerable de veces y así reducir costos economicos, comparada con capsulas no reutilizables. Aun así diferentes versiones de la misma la hicieron capas de realizar multiples tareas de carga, incluido el reabastecimiento de la estacion internacional y el reingreso a la tierra de muestras desde esta. El notable exito de esta capsula resulto en un contrato con SpaceX  para entregar la carga a la ISS en virtud de la "NASA 's comercial reabastecimiento Servicios programa" y dragón comenzó vuelos regulares de carga en octubre de 2012

![](https://upload.wikimedia.org/wikipedia/commons/b/bc/COTS2Dragon.6.jpg)
**Figura 1**. La nave de carga comercial SpaceX Dragon se acerca a la Estación Espacial Internacional el 25 de mayo de 2012 para atracar. Los ingenieros de vuelo Don Pettit y Andre Kuipers enfrentaron a Dragon a las 9:56 am (EDT) con el brazo robótico Canadarm2 y usaron el brazo robótico para atrapar a Dragon a las 12:02 pm del 25 de mayo , 2012. .By NASA - http://spaceflight.nasa.gov/gallery/images/station/crew-31/html/iss031e071134.html, Public Domain, https://commons.wikimedia.org/w/index.php?curid=19820442

### Aspectos generales
La nave Dragon es capaz de transportar hasta 7 pasajeros hacia y desde la órbita terrestre, y más allá. La sección presurizada de la cápsula está diseñada para transportar personas y carga ambientalmente sensible. 
Hacia la base de la cápsula y dentro del cono de la nariz están los propulsores Draco, que permiten maniobras orbitales. El tronco del dragón transporta carga sin presión y permanece unido al Dragón hasta poco antes de volver a entrar en la atmósfera de la Tierra.

## Versiones
SpaceX creo diferentes versiones de la nave Dragon especificas para cada propósito en particular.

### Dragon CRS.

El SpaceX CRS-1, también conocido como SPX-1, fue el tercer vuelo para SpaceX  de nave de carga Dragón sin tripulación, el cuarto vuelo general de "dos etapas Falcon 9" de vehículos de lanzamiento de la compañía, y la primera misión operacional SpaceX bajo su contrato Comercial de Re abastecimiento de Servicios con la NASA. 

El contrato comercial de spaceX con la nasa para reabastecer y descargar la Estación Internacional ISS, provocó una linea de misiones sin tripulación para la nave Dragon. Esta linea de misiones fue bautizada como Dragon CRS (Comercial Resupply Service).

![](https://upload.wikimedia.org/wikipedia/commons/2/21/SpaceX_CRS-1_Patch.png)
**Figura 2** .Logo de la serie de misiones "Dragon CRS". De NASA - http://www.collectspace.com/ubb/Forum18/HTML/001031.html, Dominio público, https://commons.wikimedia.org/w/index.php?curid=36663148


La cápsula CRS Dragon puede transportar 3.310 kg de carga, que puede estar a presión, sin presión o en cualquier lugar. Puede regresar a la Tierra 3.310 kg, que puede ser toda la masa de eliminación sin presión, o hasta 2.500 kg de carga presurizada de retorno, impulsada por limitaciones de paracaídas. Existe una restricción de volumen de carga no presurizada en el maletero de 14 metros cúbicos y 11.2 metros cúbicos de carga presurizada.  El tronco posee paneles solares que producen una potencia máxima de 4 kW.  El diseño del Dragón CRS se modificó a partir del quinto vuelo del Dragón. Si bien la línea de molde exterior del Dragón no cambió, los estantes de aviónica y carga se rediseñaron para suministrar sustancialmente más energía eléctrica a dispositivos de carga motorizados, incluidos los módulos de congelador GLACIER y MERLIN para transportar cargas científicas críticas.

### Dragon Lab.


Cuando se usa para vuelos comerciales que no son de la NASA ni de la EEI, la versión sin tripulación de la nave espacial Dragon se llama DragonLab. Es reutilizable y de vuelo libre y puede transportar cargas presurizadas y no presurizadas. Sus subsistemas incluyen propulsión, potencia, control térmico y ambiental, aviónica, comunicaciones, protección térmica, software de vuelo, sistemas de guía y navegación, y equipos de entrada, descenso, aterrizaje y recuperación. Tiene una masa acumulada total combinada de 6,000 kilogramos en el lanzamiento, y una masa descendente máxima de 3,000 kilogramos cuando regresa a la Tierra.



### Dragon 2. Tripulación y carga



SpaceX ha desarrollado un sucesor de Dragon llamado Dragon 2, diseñado para transportar pasajeros y tripulación. Podrá transportar hasta siete astronautas, o alguna combinación de tripulación y carga, hacia y desde la órbita terrestre baja. El escudo térmico Dragon 2 está diseñado para soportar las velocidades de reentrada de la Tierra desde los vuelos espaciales lunares y marcianos. SpaceX se comprometió con varios contratos del Gobierno de los EE. UU. Para desarrollar la variante tripulada Dragon 2. La fase 2 del contrato CRS se realizará utilizando la variante Dragon 2 Cargo que carece de ventanas y asientos.

![](https://upload.wikimedia.org/wikipedia/commons/e/e2/SpaceX_Dragon_v2_Pad_Abort_Vehicle_%2816661791299%29.jpg)
**Figura 3**. Exterior del Dragon 2. By SpaceX - https://www.flickr.com/photos/spacexphotos/16661791299/, CC0, https://commons.wikimedia.org/w/index.php?curid=39138034

![](https://upload.wikimedia.org/wikipedia/commons/9/93/Dragon_V2_interior.2.jpg)
**Figura 4**. Interior del Dragon 2. By NASA/Dmitri Gerondidakis - http://mediaarchive.ksc.nasa.gov/detail.cfm?mediaid=69276, Public Domain, https://commons.wikimedia.org/w/index.php?curid=33071054

# Dragon rojo
Red Dragon era una versión de la nave espacial Dragon que previamente se había propuesto volar más allá de la órbita de la Tierra y transitar a Marte a través del espacio interplanetario . Además de los propios planes privados de SpaceX para una eventual misión a Marte, el Centro de Investigación Ames de la NASA había desarrollado un concepto llamado Dragón Rojo: una misión a Marte de bajo costo que usaría Falcon Heavy como vehículo de lanzamiento y vehículo de inyección transmarciano, y una Cápsula basada en Dragon 2 para entrar en la atmósfera de Marte . El concepto fue originalmente concebido para su lanzamiento en 2018 como una misión de Discovery de la NASA., luego alternativamente para 2022, pero nunca se presentó formalmente para su financiación dentro de la NASA.  La misión habría sido diseñada para devolver muestras de Marte a la Tierra a una fracción del costo de la propia misión de devolución de muestras de la NASA, que se proyectó en 2015 con un costo de $ 6 mil millones.



El 27 de abril de 2016, SpaceX anunció su plan de seguir adelante y lanzar un módulo de aterrizaje de Dragón modificado a Marte en 2018. Sin embargo, Musk canceló el programa Red Dragon en julio de 2017 para centrarse en el desarrollo del sistema de la nave espacial. La cápsula modificada del Dragón Rojo habría realizado todas las funciones de entrada, descenso y aterrizaje (EDL) necesarias para entregar cargas útiles de 1 tonelada o más a la superficie marciana sin usar un paracaídas. El análisis preliminar mostró que el arrastre atmosférico de la cápsula lo ralentizaría lo suficiente como para que la etapa final de su descenso esté dentro de las capacidades de sus propulsores de retro propulsión SuperDraco.

### Dragón XL 
La Plataforma Orbital Lunar Gateway (LOP-G por sus siglas en inglés, Lunar Orbital Platform), también conocida como Lunar Gateway o simplemente Gateway, es una estación espacial propuesta en órbita lunar destinada a servir como centro de comunicaciones alimentado por energía solar, laboratorio de ciencias, módulo de habitación a corto plazo y área de espera para rovers y otros robots.

Si bien el proyecto está liderado por la NASA, el Gateway está destinado a ser desarrollado, mantenido y utilizado en colaboración con socios comerciales e internacionales. Servirá como punto de partida para la exploración robótica y tripulada del polo sur lunar, y es el punto de partida propuesto para el concepto de transporte espacial profundo de la NASA para el transporte a Marte.



![](https://www.nasaspaceflight.com/wp-content/uploads/2020/03/4519DA52-9E20-4E0F-8859-5A211AED9B7B-2048x1152.jpeg)

**Figura 5**. Concepto de Estación Gatway. El modulo Dragon XL se insertaría en esta para reabastecer la estación. De https://www.nasaspaceflight.com/2020/03/dragon-xl-nasa-spacex-lunar-gateway-supply-contract/.



El 27 de marzo de 2020, SpaceX reveló la nave espacial de re abastecimiento Dragon XL para transportar carga presurizada y no presurizada, experimentos y otros suministros al Lunar Gateway planeado por la NASA. El equipo entregado por las misiones Dragon XL podría incluir materiales de recolección de muestras, trajes espaciales y otros artículos que los astronautas puedan necesitar en el Gateway y en la superficie de la Luna, según la NASA. Se lanzará en cohetes SpaceX Falcon Heavy desde la plataforma 39A en el Centro Espacial Kennedyen Florida. El Dragon XL permanecerá en el Gateway durante seis a 12 meses a la vez, cuando las cargas útiles de investigación dentro y fuera del buque de carga se puedan operar de forma remota, incluso cuando las tripulaciones no estén presentes. Se espera que su capacidad de carga útil sea más de 5,000 kilogramos a la órbita lunar.


# Especificaciones de la capsula Dragon



El módulo Dragon tiene una carga útil de 6000kg al momento de lanzamiento, y 3000kg al reingresar a la atmósfera. Físicamente tiene un diametro de 4m/13ft, y una altura total de 8,1m/26,7ft. Este volumen está dividido en dos partes principales:
- La capsula.
- El tronco. (Trunk)

![](https://ichef.bbci.co.uk/news/624/cpsprodpb/11F22/production/_105860537_space2_x_dragon_capsule_inf6.png)

**Figura 1** . Imagen del modulo, con escala y descripción de sus partes. Fuente: https://www.bbc.com/news/science-environment-51169705

## La capsula:

La capsula se mantiene presurizada, y está diseñada para transportar astronautas y material o instrumental sensible, con un volumen total de 9.3m^3/328ft^3. 
Sobre los laterales de la capsula están los impulsores (Thrusters), 16 cohetes hipergólicos modelo "Draco" que ayudan al modulo a posicionarse y maniobrar en el vacío del espacio. Cada uno de ellos tiene un impulso de 400 N/ 90 lbf en vacío.

Tambien se dispone de 8 cohetes "SuperDraco". La fuerza que cada uno de estos puede generar es de 73 kN / 16,400 lbf, pero solo son utilizados en caso de emergencia para evacuar a la tripulación en situaciones de emergencia.  

![](https://upload.wikimedia.org/wikipedia/commons/thumb/9/92/SuperDraco_rocket_engines_at_SpaceX_Hawthorne_facility_%2816789102495%29.jpg/245px-SuperDraco_rocket_engines_at_SpaceX_Hawthorne_facility_%2816789102495%29.jpg)

**Figura 2** . Imagen de los cohetes SuperDraco en la instalación de SpaceX Hawthorne. Fuente: https://es.wikipedia.org/wiki/SuperDraco

## El tronco: 

El tronco se encuentra en la parte inferior del módulo Dragon, y sobre esta se encuentran montados paneles solares y sistemas para asistir al módulo durante el lanzamiento. Dentro de esta parte del módulo se transporta carga no sensible. El tronco se desacopla del módulo cuando este reingresa a la atmósfera.

# Aspectos del diseño

### Escudo térmico



Los vehículos típicos que se someten al proceso de ingreso o egreso de un planeta, que por lo general es la Tierra, deben encontrar una solución a las elevadas temperaturas que se generan por la fricción con la atmósfera. Estos vehículos también pueden alcanzar temperaturas elevadas por otros medios, ya sea la cercanía al sol o algún tipo de calentamiento producto de sus partes componentes, pero generalmente no son temperaturas tan elevadas como la alcanzada en el reingreso a la atmósfera. Se requiere métodos especiales de protección contra el calentamiento y para esto se han desarrollado varias tecnologías avanzadas para permitir la reentrada atmosférica y el vuelo a velocidades extremas.

Un sistema de protección térmica o TPS es la barrera que protege una nave espacial durante el abrasador calor de la reentrada atmosférica. Un objetivo secundario puede ser proteger la nave espacial contra el calor y el frío del espacio mientras está en órbita. Se utilizan múltiples enfoques para la protección térmica de naves espaciales, entre ellos escudos térmicos ablativos, enfriamiento pasivo y enfriamiento activo de superficies de naves espaciales.

#### Pica-X

Hasta hace algunos años, los vehículos espaciales usaban un escudo térmico que recubría las paredes de los sectores más expuestos, echo de un materia especial llamado ablativo, capaz de resistir altas temperaturas, el material desarrollado por la NASA utilizado en sus trasbordadores y sus capsulas es un material ablativo de carbono impregnado de fenol (llamado PICA). Una versión mejorada y más fácil de producir un escudo térmico llamada PICA-X fue desarrollada por SpaceX en 2006-2010 para la cápsula del espacio del dragón. La primera prueba de reentrada de un escudo térmico PICA-X  fue en la misión Dragon C1 el 8 de diciembre de 2010. El escudo térmico PICA-X fue diseñado, desarrollado y totalmente calificado por un pequeño equipo de sólo una docena de ingenieros y técnicos en menos de cuatro años. PICA-X es diez veces menos costoso de fabricar que el material de escudo térmico de la NASA PICA. Este nuevo material es capas de resistir una temperatura exterior de hasta 1.700 ºC, a la vez que mantiene la temperatura interior a menos de 170ºC.

#### Sistema de control de reacción

Un sistema de control de reacción (RCS por reaction control system) es un sistema de un vehículo espacial que utiliza propulsores con toberas para controlar la actitud, y a veces traslación. También se denomina así al uso de cambios de la dirección del empuje del motor para proveer control de actitud estable de un vehículo con despegue o aterrizaje vertical o en una distancia corta, por debajo de velocidades convencionales para vuelos con alas. Un RCS posee la capacidad de proveer pequeñas cantidades de impulso en la dirección o combinación de direcciones que se desee. Un SCR es capaz de proveer torque para controlar la rotación (cabeceo, alabeo, y guiñada). Debido a que los vehículos espaciales cuentan con una cantidad limitada de combustible y las posibilidades de recargarlo son muy limitadas, es que se han desarrollado sistemas de control de reacción alternativos que permiten conservar el combustible. Para mantenimiento de órbita, algunos vehículos espaciales (particularmente aquellos en órbita geosincrónica) utilizan motores de alto impulso específico tales como arcjets, propulsores iónicos, o propulsores a efecto Hall para controlar la orientación, algunos vehículos espaciales utilizan ruedas de reacción que rotan para controlar las velocidades de giro del vehículo.
Space x utiliza una familia de propulsores llamados "SpaceX Draco". Son motores de cohetes líquidos hipergólicos diseñados y construidos por SpaceX para su uso en sus cápsulas espaciales . Hasta la fecha se han construido dos tipos de motores: Draco y SuperDraco.
El propulsor Draco original es un pequeño motor de cohete para usar en la nave espacial Dragon con el fin de realizar maniobras de rotación y algunas de menor grado de movimiento traslacional.
SuperDraco se deriva de Draco y utiliza el mismo propulsor almacenable (no criogénico) que los pequeños propulsores Draco, pero es mucho más grande y entrega más de 100 veces el empuje . Los motores SuperDraco se están utilizando en la nave espacial Crew Dragon para proporcionar capacidad de lanzamiento y escape a órbita terrestre baja .

#### Sistema de Recuperación.
Las naves dragon son reutilizables y pueden regresar a la Tierra 3.500 kg (7.700 lb), que pueden ser toda la masa de eliminación sin presión o hasta 3.000 kg (6.600 lb) de carga presurizada de retorno de la EEI. La nave cuenta con un sistema de protección superior e inferior basado en la tecnología PICA, llamado PICA-X, capas de resistir las temperaturas de ingreso a la atmósfera. Luego una serie de propulsores SuperDraco y paracaídas frenan su velocidad de caída. La capsula dragon esta diseñada para realizar su amerizaje en el océano, ya sea el Atlántico o en el Pacífic


# Vinculación con la Estación Espacial Internacional: aspectos más importantes e hitos.

En 2005, a traves de su Programa Comercial de Transporte Orbital (COTS, por sus siglas in ingles), NASA solicitó propuestas para retirar el transbordador espacial y reemplazarlo por transportes comerciales para reabastecer la Estación Espacial Internacional (ISS). El módulo Dragon fue parte de la propuesta de SpaceX, enviada a NASA en Marzo del 2006, y siendo elegida en Agosto del mismo año para enviar cargamento a la ISS. 

## Lanzamientos de demostración

En los términos del contrato se solicitaron tres lanzamientos de prueba, aunque esto fue reducido a dos al combinar los requisitos del tercer lanzamiento con el segundo. En la primera misión, lanzada en Junio de 2010, se utilizó una versión de la capsula Dragon diseñada para pruebas, cuyo proposito fue transmitir información aerodinámica sobre el ascenso. Este módulo no sobrevivió el reingreso a la atmosfera.

El segundo lanzamiento fue realizado con éxito en Mayo de 2012, y ejecutó pruebas de sus sistemas de navegación y procesos de emergencia. Luego, fue acoplado con éxito a la Estación Espacial y descargar los materiales transportados. En esta etapa, el diseño del módulo Dragon ya había sido perfeccionado, y fue recuperado en el Océano Pacífico con exito. En Agosto de este mismo año, se anunció oficialmente que SpaceX cumplió con los términos asociados a los lanzamientos de prueba, y oficialmente fueron contratados para las misiones de reabastecimiento de la ISS.

## Servicios de Recarga Comercial, Fase 1:

En Diciembre de 2008, SpaceX cerró un trato de $1.6 billones de dolares, extendible a $3.1 billones, con NASA asegurando sus servicios por 12 vuelos de envío de provisiones a la ISS por un minimo de 20000 kg / 44000 lb. En 2009, SpaceX anunció que sus escudos térmicos pasaron las pruebas requeridas para el lanzamiento, y en Octubre de 2012 se lanzó la misión "SpaceX CRS-1", transportando el primer cargamento de provisiones para la ISS con éxito. 

Durante Marzo de 2015 se anunció que se concedió a SpaceX tres misiones adicionales de reabastecimiento, denominadas "SpaceX CRS-13" a "SpaceX CRS-15", bajo una extensión del contrato inicial, y que cubrirían las necesidades de la ISS hasta 2017. Posteriormente, en Febrero de 2016, se anunció una extensión adicional de cinco misiones, "SpaceX CRS-16" a "SpaceX CRS-20". 
# Misión de servicios de reabastecimiento comercial CRS1 a 6
El 23 de diciembre de 2008, la NASA otorgó un contrato de Servicios de re abastecimiento comercial (CRS) de $ 1.6 mil millones a SpaceX, con opciones de contrato que podrían aumentar el valor máximo del contrato a $ 3.1 mil millones.  El contrato requería 12 vuelos, con un mínimo general de 20,000 kg (44,000 lb) de carga para ser transportado a la EEI.  Se basan en misiones no tripuladas de re abastecimiento de los recursos y recuperación de desechos y materiales de la Estación espacial internacional (EEI), a través de las naves Dragon-CRS. Esta es la lista de las primeras seis misiones realizadas.

####  Dragon CRS-1
SpaceX CRS-1, fue el tercer vuelo para la nave espacial de carga Dragon sin tripulación de SpaceX , el cuarto vuelo general para el vehículo de lanzamiento Falcon 9 de dos etapas de la compañía , y el primero Misión operacional SpaceX bajo su contrato de Servicios de re abastecimiento comercial con la NASA . El lanzamiento se produjo el 8 de octubre de 2012 a las 00:34:07 UTC y duro 20 días, 18 horas, 47 minutos. La nave fue recuperada el 28 de octubre de 2012, 19:22.
Se llenó con aproximadamente 905 kilogramos (1.995 lb) de carga, 400 kilogramos (880 lb) sin embalaje.  Se incluyeron 118 kilogramos (260 lb) de suministros para la tripulación, 117 kilogramos (258 lb) de materiales críticos para respaldar los 166 experimentos a bordo de la estación y 66 nuevos experimentos, así como 105.2 kilogramos (232 lb) de hardware para la estación, así como otros artículos diversos.

El Dragón devolvió 905 kilogramos (1.995 lb) de carga, 759 kilogramos (1.673 lb) sin embalaje. Se incluyeron 74 kilogramos (163 lb) de suministros para la tripulación, 393 kilogramos (866 lb) de experimentos científicos y hardware del experimento, 235 kilogramos (518 lb) de hardware de la estación espacial, 33 kilogramos (73 lb) de equipo de traje espacial y 25 kilogramos (55 lb) de artículos varios.

![](https://upload.wikimedia.org/wikipedia/commons/b/b8/SpaceX_CRS-1_approaches_ISS-cropped.jpg)
Foto de la nave Dragon CRS-1 tomada desde la estación internacional.

#### Dragon CRS-2
SpaceX CRS-2, fue el cuarto vuelo para la nave espacial de carga Dragon sin tripulación de SpaceX, el quinto y último vuelo para el vehículo de lanzamiento Falcon 9 v1.0, de dos etapas de la compañía, y el segundo La misión operacional SpaceX contratada por la NASA bajo un contrato de Servicios de re abastecimiento comercial (CRS).
El lanzamiento ocurrió el 1 de marzo de 2013.  Un problema técnico menor en la nave espacial Dragon que involucraba las vainas de propulsión RCS ocurrió al alcanzar la órbita, pero fue recuperable.  El vehículo fue liberado de la estación el 26 de marzo de 2013 a las 10:56 UTC y cayó al Océano Pacífico a las 16:34 UTC, con un tiempo de misión de 25 días, 1 hora, 24 minutos.

Cuando se lanzó el CRS-2 Dragon se llenó con aproximadamente 1.493 lb (677 kg) de carga, 1.268 lb (575 kg) sin embalaje. Se incluyen 178 lb (81 kg) de suministros para la tripulación, 766 lb (347 kg) de experimentos científicos y hardware para experimentos, 298 lb (135 kg) de hardware para la estación y otros artículos diversos, entre ellos un La copia en CD de la canción " Up in the Air " de la banda de rock Thirty Seconds to Mars, se estrenó a bordo de la Estación Espacial Internacional el 18 de marzo de 2013, durante una transmisión de televisión de la NASA desde la estación. Los dos accesorios de agarre del subsistema de rechazo de calor (HRSGF) tenían un peso combinado de 487 lb (221 kg) y fueron transportados a la ISS dentro del tronco del Dragón sin presión como carga externa. 

El Dragón devolvió 3.020 lb (1.370 kg) de carga, 2.668 lb (1.210 kg) sin embalaje. Se incluyen 210 lb (95 kg) de suministros para la tripulación, 1.455 lb (660 kg) de experimentos científicos y hardware para experimentos, 884 lb (401 kg) de hardware para estaciones espaciales, 84 lb (38 kg) de equipos para trajes espaciales y otros artículos diversos.
![](https://upload.wikimedia.org/wikipedia/commons/2/2a/SpX_CRS-2_berthing.jpg)
Foto de la nave Dragon CRS-2 tomada desde la estación internacional.

#### Dragon CRS-3
SpaceX CRS-3, fue una misión del Servicio de Re abastecimiento Comercial a la Estación Espacial Internacional, contratada por la NASA, que fue lanzada el 18 de abril de 2014 y reingreso en el Océano Pacífico frente a la costa de California a las 19 : 05 UTC del 18 de mayo, teniendo 29 días, 23 horas, 38 minutos de tiempo de misión. Fue el quinto vuelo del Dragón desatornillado de SpaceX la nave espacial de carga y la tercera misión operativa SpaceX contrataron a la NASA bajo un contrato de Servicios de re abastecimiento comercial.
Este fue el primer lanzamiento de una cápsula Dragon en el vehículo de lanzamiento Falcon 9 v1.1 , ya que los lanzamientos anteriores usaban la configuración más pequeña v1.0 . También fue la primera vez que el F9 v1.1 ha volado sin un carenado de carga útil , y la primera prueba de vuelo experimental de un aterrizaje oceánico de la primera etapa en una misión NASA / Dragón.

Entre otras cargas de la NASA, incluidas las piezas de reparación para la ISS, la misión SpaceX CRS-3 llevó una gran cantidad de experimentos a la estación espacial, incluyendo:



- Cámaras de visión terrestre de alta definición (HDEV): cuatro cámaras de video HD comerciales que filmarán la Tierra desde múltiples ángulos diferentes desde la posición ventajosa. El experimento ayudará a la NASA a determinar qué cámaras funcionan mejor en el entorno hostil del espacio. 

- La carga útil óptica para Lasercomm Science (OPALS) demostrará comunicaciones láser de espacio a tierra de gran ancho de banda.

- Activación de células T en el espacio (TCAS): estudio de cómo "las deficiencias en el sistema inmunitario humano se ven afectadas por un entorno de micro gravedad" 

- Sistema de producción de verduras: para permitir el crecimiento de la lechuga a bordo para la investigación científica, la purificación del aire y, en última instancia, el consumo humano. La prueba de validación de hardware Veg-01 incluye una cámara de crecimiento de la planta en la cual la lechuga se cultiva en almohadas tipo fuelle con iluminación LED. 

- Un par de patas para el prototipo Robonaut 2 que ha estado a bordo de la estación espacial desde su lanzamiento en STS-133 en 2011

- Proyecto MERCCURI, un proyecto que examina la diversidad microbiana del entorno construido en la tierra y en la Estación Espacial Internacional.

![](https://upload.wikimedia.org/wikipedia/commons/e/e3/Arrival_of_CRS-3_Dragon_at_ISS_%28ISS039-E-013475%29.jpg)
Foto de la nave Dragon CRS-3 tomada desde la estación internacional

Los 1.600 kilogramos (3.500 lb) de carga descendente, se recuperaron exitosamente.Aun así, Se encontró agua dentro de la cápsula del Dragón, pero los controles preliminares indicaron que no se había dañado ningún equipo científico. La fuente del agua no se ha confirmado.

#### Dragon CRS-4
SpaceX CRS-4, fue una misión del Servicio de re abastecimiento comercial a la Estación Espacial Internacional, contratada por la NASA, que se lanzó el 21 de septiembre de 2014, llegó a la estación espacial el 23 de septiembre de 2014 y regreso el 25 de octubre de 2014, 19:39  UTC, con un tiempo total de misión de 34 días, 13 horas, 46 minutos.Fue el sexto vuelo de SpaceX sin tripulación de la nave dragón y la cuarta misión operativa SpaceX  bajo un Contrato de sercicio y reabastecimiento para la Nasa. La misión llevó equipos y suministros a la estación espacial, incluida la primera impresora 3D que se probó en el espacio, un dispositivo para medir la velocidad del viento en la Tierra y pequeños satélites que se lanzarán desde la estación. También trajo 20 ratones para investigación a largo plazo a bordo de la EEI.
La carga útil fue compuesta por 4,885 lb (2,216 kg) de carga, incluyendo 1,380 lb (630 kg) de suministros para la tripulación.  La carga incluía el ISS-RapidScat , un dispersómetro diseñado para soportar el pronóstico del tiempo haciendo rebotar las microondas en la superficie del océano para medir la velocidad del viento, que se lanzó como una carga útil externa que se unirá al final del laboratorio de la estación de Columbus. CRS-4 también incluye el Lanzador cinético integrado de la estación espacial para sistemas de carga útil orbital (SSIKLOPS), que proporcionará aún otro medio para liberar otros satélites pequeños de la EEI. Además, CRS-4 llevó una nueva instalación permanente de investigación de ciencias biológicas a la estación: la carga útil del Densitómetro Óseo (BD), desarrollada por Techshot, que proporciona una capacidad de escaneo de densidad ósea en la ISS para su uso por la NASA y el Centro para el Avance de Ciencia en el espacio (CASIS). El sistema mide la densidad mineral ósea (y el tejido magro y graso) en ratones usando Absorciometría de rayos X de energía dual (DEXA) . El Sistema de Hardware de Investigación de Roedores también fue llevado a la EEI como parte de la carga útil.

El núcleo estructural de la cápsula Dragon CRS-4, C106 , fue restaurado y reutilizado en la misión SpaceX CRS-11 , la primera cápsula Dragon en ser reutilizada.

![](https://upload.wikimedia.org/wikipedia/commons/4/40/SpaceX_CRS-4_Dragon.jpg)
Foto de la nave Dragon CRS-4 tomada desde la estación internacional

#### Dragon CRS-5
SpaceX CRS-5, fue una misión del Servicio de re abastecimiento comercial a la Estación Espacial Internacional, realizada por SpaceX para la NASA, y se lanzó el 10 de enero de 2015 y finalizó el 11 de febrero de 2015, con un tiempo de misión de 31 días, 14 horas, 56 minutos. Fue el séptimo vuelo de La nave espacial de carga Dragon sin tripulación de SpaceX y la quinta misión operacional de SpaceX bajo un contrato de servicios de re abastecimiento de la ISS. 
La nave transportó 2,317 kilogramos (5,108 lb) de carga a la ISS. En esto se incluyeron 490 kg (1,080 lb) de provisiones y equipo para la tripulación, 717 kg (1,581 lb) de hardware de la estación, 577 kg (1,272 lb) de equipo y experimentos científicos, y el aerosol de nube de  Sistema de transporte (Cats) 494 kg (1,089 lb). 

Al finalizar su estadía, Dragon fue cargado con 1.332 kg (2.937 lb) de carga saliente, devolviéndolo a la Tierra.

![](https://upload.wikimedia.org/wikipedia/commons/c/c4/CRS-5_Dragon_on_approach_to_ISS_%28ISS042-E-119867%29.jpg)
Foto de la nave Dragon CRS-5 tomada desde la estación internacional

#### Dragon CRS-6
SpaceX CRS-6, fue una misión del Servicio de re abastecimiento comercial a la Estación Espacial Internacional, contratada por la NASA. Fue el octavo vuelo de SpaceX sin tripulación, de la nave espacial de carga Dragón, y la sexta misión operativa contratada para la NASA en virtud del contrato comercial de re abastecimiento Servicios. La misión empezó el 14 de abril de 2015, 20:10:41 UTC  y finalizo el  21 de mayo de 2015, 16:42  UTC, contando un tiempo de misión de 36 días, 20 horas, 31 minutos.
La nave espacial Dragon se llenó con 2015 kg (4,387 libras) de suministros y cargas útiles, incluidos materiales críticos para apoyar directamente alrededor de 40 de las más de 250 investigaciones científicas y de investigación que ocurrirán durante las Expediciones 43 y 44.  Entre otros artículos a bordo:



- La compañía Planetary Resources transportó un Arkyd 3, conocido como Arkyd 3 Reflight, a la ISS a bordo de Dragon en CRS-6, en un intento de validar y madurar la tecnología de su serie de naves espaciales Arkyd. Este es el segundo satélite Arkyd 3.

- La compañía Planet Labs transportó 14 satélites Cubesat de observación de la Tierra Flock-1e para su posterior despliegue desde la estación espacial a través de un acuerdo con NanoRacks, operador del Centro para el Avance de la Ciencia en el Espacio (CASIS). 

Dragon devolvió 1.370 kg (3.020 lb) de carga a la Tierra.

![](https://upload.wikimedia.org/wikipedia/commons/7/7b/CRS-6_Dragon_from_ISS_%28ISS043-E-122200%29.jpg)
Foto de la nave Dragon CRS-6 tomada desde la estación internacional 

# Programa Comercial de Tripulación

![](https://upload.wikimedia.org/wikipedia/commons/thumb/e/e9/Commercial_Crew_Program_logo_-_white_background.png/220px-Commercial_Crew_Program_logo_-_white_background.png)

**Figura 1** . Logo oficial del Programa Comercial de Tripulación. Fuente: https://es.wikipedia.org/wiki/Desarrollo_de_tripulaci%C3%B3n_comercial

El Programa Comercial de Tripulación, ó "CCP" por sus siglas en inglés, es un programa administrado por la NASA que busca financiar y asistir al desarrollo de transportes espaciales capaces de transportar astronautas a la Estación Espacial Internacional (ISS) y otros destinos en órbita baja de la tierra de forma segura.

El enfoque del programa facilita el desarrollo de numerosos diseños de naves espaciales capaces de ser usadas para enviar astronautas al espacio a elección. Para lograr esto, NASA permite el uso de sus recursos e información durante el desarrollo de las naves a las compañías aeroespaciales que participan de este programa. 

Para poder ingresar a este programa, la NASA postula varios requisitos que deben ser cumplidos por los transportes a desarrollar. Estos son:
- Deben poder cumplir con al menos dos viajes de ida y vuelta de tripulaciones de cuatro astronautas y equipo a la estación espacial.
- Se debe garantizar la seguridad de la tripulación en casos de emergencia tanto en la plataforma de lanzamiento y durante el ascenso a órbita. 
- La nave espacial debe poder servir como refugio para los astronautas por 24 horas en caso de emergencia, y ser capaz de estar acoplada a la Estación Espacial Internacional durante al menos 210 días.

El programa además busca indirectamente hacer crecer los posibles mercados espaciales, y no pone limitaciones a las empresas para vender vuelos a clientes privados. El costo de desarrollo de las naves se divide entonces en la financiación de parte del Gobierno de Los Estados Unidos (A traves de NASA) y de financiación personal por parte de clientes privados.

# Descripción de DragonLab
Cuando se usa para vuelos comerciales que no son de la NASA ni de la EEI, la versión sin tripulación de la nave espacial Dragon se llama DragonLab . Es reutilizable y de vuelo libre y puede transportar cargas presurizadas y no presurizadas. Sus subsistemas incluyen propulsión, potencia, control térmico y ambiental , aviónica , comunicaciones, protección térmica , software de vuelo, sistemas de guía y navegación , y equipos de entrada, descenso, aterrizaje y recuperación. Tiene una masa acumulada total combinada de 6,000 kilogramos (13,000 lb) en el lanzamiento, y una masa descendente máxima de 3,000 kilogramos (6,600 lb) cuando regresa a la Tierra. Se puede utilizar como una plataforma de investigación de microgravedad, como para materiales, fluidos, física de combustión, biotecnología o experimentos de ciencias de la vida. También se puede utilizar como demostrador de tecnología para desarrolladores de instrumentos y sensores.

![](https://www.bis-space.com/wp-content/uploads/2011/10/SpaceX-DragonLab.jpg) 

Imagen generada por computadora. Ciertos problemas en la empresa y el surgimiento del contrato CRS, provocaron que el proyecto quedara de lado, aunque volverá a surgir la propuesta en los próximos años.

# Demandas ingenieriles del Dragon.

El diseño de un vehiculo espacial es un proyecto de gran magnitud con requisitos considerables en muchas areas de las ciencias físicas, y requiere el trabajo en conjunto de profesionales en muchas areas. A continuación elaboraremos a grandes rasgos sobre dos de estos requisitos desde la perspectiva de estudiantes de Ingeniería Mecánica, y sobre las materias en nuestro trayecto académico que se relacionen a los puntos destacados.

Todo vehiculo espacial que entre o salga de la atmósfera sufrirá ante fuerzas asociadas a problemas despreciables en condiciones menos complejas, tales como calentamiento por fricción con el aire, la aerodinámica del modelo empleado, entre otros. Tambien es importante destacar que las condiciones ambientales y climáticas no son despreciables en la salida ni el reingreso a la atmósfera. Debido a esto, se diseñan escudos térmicos con materiales apropiados para proteger al vehiculo durante su vuelo. El diseño de estos escudos fue uno de los obstaculos más importantes historicamente para el desarrollo de vehiculos espaciales, debido a que los materiales disponibles no eran eficientes y resultarían en estructuras demasiado masivas para ser viables. Algunas materias del programa de Ingeniería Mecánica que pueden relacionarse al desarrollo de materiales más optimos son 'Materiales I' y 'Materiales II', así como la materia optativa 'Tecnología de Materiales Avanzados'

El segundo problema que observamos, a grandes rasgos, es la capacidad de la capsula de maniobrar en el espacio y la eficiencia del combustible que posee para hacerlo. Se requiere un balance entre eficiencia y masa para poder maniobrar correctamente sin entorpecer el lanzamiento por peso excesivo. Para este desarrollo la dirección de un ingeniero químico es ideal, pero el ingeniero mecánico puede involucrarse en el diseño de los motores que utilicen el combustible empleando los conocimientos aprendidos en 'Mecánica aplicada a las máquinas', 'Cálculo de elementos de máquinas' y 'Tecnología Mecánica'.


# Bibliografía / enlaces a sitios web:

- https://www.spacex.com/dragon
- https://en.wikipedia.org/wiki/SpaceX_Dragon
- https://es.wikipedia.org/wiki/SpaceX_CRS-1
- https://es.wikipedia.org/wiki/Plataforma_Orbital_Lunar_Gateway
- https://danielmarin.naukas.com/2020/03/28/dragon-xl-la-nueva-nave-de-spacex-para-llevar-carga-a-la-estacion-lunar-gateway/
- https://www.nasaspaceflight.com/2020/03/dragon-xl-nasa-spacex-lunar-gateway-supply-contract/
﻿- https://www.bis-space.com/2011/10/26/2972/spacex-dragonlab
- https://en.wikipedia.org/wiki/SpaceX_Dragon
- https://en.wikipedia.org/wiki/SpaceX_Dragon#Pressure_vessel
- https://es.wikipedia.org/wiki/Reentrada_atmosf%C3%A9rica
- https://www.technologyreview.es/s/11269/los-materiales-de-ensueno-de-la-nasa-para-recorrer-el-sistema-solar
- https://www.cofis.es/pdf/fys/fys18/fys_18_44-47.pdf
- https://en.wikipedia.org/wiki/SpaceX_Dragon
- https://es.wikipedia.org/wiki/Sistema_de_control_de_reacci%C3%B3n
- https://nmas1.org/news/2019/03/08/crew-dragon-reingreso-atmosfera
- https://en.wikipedia.org/wiki/SpaceX_Dragon- https://en.wikipedia.org/wiki/SpaceX_Dragon#Pressure_vessel
- https://es.wikipedia.org/wiki/Reentrada_atmosf%C3%A9rica
- https://www.technologyreview.es/s/11269/los-materiales-de-ensueno-de-la-nasa-para-recorrer-el-sistema-solar
- https://www.cofis.es/pdf/fys/fys18/fys_18_44-47.pdf
- https://en.wikipedia.org/wiki/SpaceX_Dragon
- https://es.wikipedia.org/wiki/Sistema_de_control_de_reacci%C3%B3n
- https://nmas1.org/news/2019/03/08/crew-dragon-reingreso-atmosfera
- https://en.wikipedia.org/wiki/SpaceX_Dragon#Pressure_vessel
- https://es.wikipedia.org/wiki/Reentrada_atmosf%C3%A9rica
- https://www.technologyreview.es/s/11269/los-materiales-de-ensueno-de-la-nasa-para-recorrer-el-sistema-solar
- https://www.cofis.es/pdf/fys/fys18/fys_18_44-47.pdf
- https://en.wikipedia.org/wiki/SpaceX_Dragon
- https://es.wikipedia.org/wiki/Sistema_de_control_de_reacci%C3%B3n
- https://nmas1.org/news/2019/03/08/crew-dragon-reingreso-atmosfera
- https://www.nasa.gov/sites/default/files/files/CCP-mini-Spanish-2.pdf
- http://crubweb.uncoma.edu.ar/cms/wp-content/uploads/2015/05/INGENIERIA-MECANICA.pdf